--[[--------------------------------------------------
CommentAutoNewLine.lua
Authors: mozers�
Version: 1.0
------------------------------------------------------
Description:
������� �� Enter � ����� ������������������ ������ ��������� ������ �����������.
���������� Issue 66 <http://code.google.com/p/scite-ru/issues/detail?id=66>
------------------------------------------------------
Connection:
 In file SciTEStartup.lua add a line:
    dofile (props["SciteDefaultHome"].."\\tools\\CommentAutoNewLine.lua")
--]]--------------------------------------------------

local function CommentAutoNewLine(char)
	if string.byte(char) ~= 10 then return end

	local current_pos = editor.CurrentPos-1
	local current_line = editor:LineFromPosition(current_pos)

	local line_start_pos = editor:PositionFromLine(current_line)
	local line_end_pos = editor.LineEndPosition[current_line]

	if IsComment(current_pos-2) then
		local comment = props["comment.block."..editor.LexerLanguage]
		local start_comment = editor:findtext(comment, 0, line_start_pos, line_end_pos)
		if start_comment ~= nil then
			local new_comment = string.rep(" ", editor.Column[start_comment])..comment.." "
			editor:ReplaceSel(new_comment)
			editor:GotoPos(current_pos + 1 + string.len(new_comment))
			return true
		end
	end
end

-- Add user event handler OnChar
local old_OnChar = OnChar
function OnChar(char)
	local result
	if old_OnChar then result = old_OnChar(char) end
	if CommentAutoNewLine(char) then return true end
	return result
end
