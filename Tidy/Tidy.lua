--[[--------------------------------------------------
Tidy.lua
Version: 1.2.2
Author: mozers�
���� � ������ ����������: Fraerok
----------------------------------------------
�������������� ����� HTML Tidy <http://tidy.sourceforge.net>
----------------------------------------------
������ �����������:
  command.mode.220.$(file.patterns.web)=subsystem:lua,savebefore
  command.220.$(file.patterns.web)=dostring config='html' mode='modify' dofile(props['tidy'])
���:
config='html' - ������������ ��������� ������������ (html, xml, word, writer)
mode='modify' - ������� ����, ��� ���� ����� ������������� (������ modify ����� ������� ����� �����)
                ���� �������� mode �����������, �� ������������ �������� ����� � ����� ��������� ������ � ������� (���� ��� ���� �� ��������������)

----------------------------------------------
����������� ��� SciTE-Ru (����� ��������� � ������� Tools\htm\Tidy ��� �������� �������������� ������):

[HTML Tidy]
# ----------------------------------------------
#HTML Tidy
tidy=$(SciteDefaultHome)\tools\Tidy\Tidy.lua

command.submenu.parent.150.$(tagfiles)=1
command.submenu.name.150.$(tagfiles)=HTML Tidy

  command.parent.150.$(tagfiles)=150
  command.name.150.$(tagfiles)=HTML verify
  command.mode.150.$(tagfiles)=subsystem:lua,savebefore
  command.150.$(tagfiles)=dostring config='html' dofile(props['tidy'])

  command.parent.151.$(tagfiles)=150
  command.name.151.$(tagfiles)=HTML modify
  command.mode.151.$(tagfiles)=subsystem:lua,savebefore
  command.151.$(tagfiles)=dostring config='html' mode='modify' dofile(props['tidy'])

  command.separator.152.$(tagfiles)=1
  command.parent.152.$(tagfiles)=150
  command.name.152.$(tagfiles)=XML verify
  command.mode.152.$(tagfiles)=subsystem:lua,savebefore
  command.152.$(tagfiles)=dostring config='xml' dofile (props["tidy"])

  command.parent.153.$(tagfiles)=150
  command.name.153.$(tagfiles)=XML modify
  command.mode.153.$(tagfiles)=subsystem:lua,savebefore
  command.153.$(tagfiles)=dostring config='xml' mode='modify' dofile (props["tidy"])

  command.separator.154.$(tagfiles)=1
  command.parent.154.$(tagfiles)=150
  command.name.154.$(tagfiles)=HTML Word modify
  command.mode.154.$(tagfiles)=subsystem:lua,savebefore
  command.154.$(tagfiles)=dostring config='word' mode='modify' dofile (props["tidy"])

  command.separator.4.$(tagfiles)=1

--]]----------------------------------------------------

local html=[[
doctype: "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"
indent: auto
indent-spaces: 2
tab-size: 2
vertical-space: 0
wrap: 0
char-encoding: raw
tidy-mark: no
quote-marks: yes
output-html: yes
]]

local xml=[[
indent: auto
indent-spaces: 4
wrap: 705032704
char-encoding: raw
tidy-mark: no
input-xml: yes
output-xml: yes
assume-xml-procins: yes
quote-marks: yes
]]

local word=[[
char-encoding:raw
wrap:0
tab-size:2
clean:yes
word-2000:yes
fix-uri:no
tidy-mark:no
alt-text:
]]

local writer=[[
char-encoding:raw
wrap:0
tab-size:2
fix-uri:no
tidy-mark:no
alt-text:
]]

---------------------------------------------------------

local path = string.gsub(props['tidy'],"[^\\]+$","")
local tidy='"'..path..'tidy.exe"'
local file_config = props['TEMP']..'\\tidy.cfg'
local file_output = props['TEMP']..'\\tidy.err'

-- ������� config.cfg
if config == 'html' then config = html end
if config == 'xml' then config = xml end
if config == 'word' then config = word end
if config == 'writer' then config = writer end
io.output(file_config)
io.write(config)
io.close()

if mode == nil then
	mode = ""
else
	mode = " -m"
end
local cmd = tidy..' -config "'..file_config..'"'..' -f "'..file_output..'"'..mode..' "'..props['FilePath']..'"'
shell.exec(cmd, nil, true, true)

-- ������ ��������� ���� � ����������� tidy � �������
local file = io.open(file_output)
local lines_table={}
for line in file:lines() do
	if line == "" then break end
	table.insert(lines_table,line)
end
file:close()

-- ��������� ��������� ������� � �������� ����������� ��������
local result = table.remove(lines_table)
if result == "No warnings or errors were found." then
	-- ���� ������ �� ������� ������������ �������� ������� ������� ������
	result = "! "..result
end

if mode == "" then
	-- ���� ���� �����������, �� � �������� ������ ��������� ������ � ��������
	result = result.."\r\n"..table.concat(lines_table, "\r\n")
    -- (���� - ��������������, �� �������� � ������� ������ � �������� �� ����� ������)
end
mode = nil
print(result)
print('Info: Read full info to file '..file_output)
