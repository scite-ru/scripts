@ECHO OFF
SET output=Show_XPM_Images.lua
PUSHD "%~dp0"
(
	echo local Images = {}
	echo.
	FOR %%I IN (*.xpm) DO (
		echo Images["%%~nI"] = [==[
		type "%%I"
		echo ]==]
		echo.
	)
) 1>%output%

rem Write Lua Script
FOR /F "tokens=1 delims=:" %%i IN ('findstr /n /b ":LuaScript" %~s0') DO (
	MORE /t4 +%%i %~s0>>%output%
)
POPD
EXIT

:LuaScript
--------------------------------------------------------

local img_sep = '#'
local lst_sep = '|'
local user_list = {}
local i = 0
editor:ClearRegisteredImages()
for fields, value in pairs(Images) do
	editor:RegisterImage(i, value)
	user_list[i+1] = i.." "..fields..img_sep..i
	i = i + 1
end

editor.AutoCTypeSeparator = string.byte(img_sep)
editor.AutoCSeparator = string.byte(lst_sep)
editor:UserListShow(8, table.concat(user_list, lst_sep))
