/*
SciTE Restarter
Version: 1.1
Author: mozers�
------------------------------------------------
Description:
  ������������� SciTE (��������� SciTE.Helper)
�����������:
	command.parent.117.*=11
	command.name.117.*=Restart SciTE
	command.117.*="$(SciteDefaultHome)\tools\Restart.js"
	command.mode.117.*=subsystem:shellexec
*/

var WshShell = new ActiveXObject("WScript.Shell");
try {
	var SciTE=new ActiveXObject("SciTE.Helper");
} catch(e) {
	WScript.Echo("Please install SciTE Helper before!");
	WScript.Quit(1);
}
var scite_path = '"'+SciTE.Props("SciteDefaultHome")+'\\SciTE.exe"';
SciTE.MenuCommand(140); //IDM_QUIT
WScript.Sleep(1500);
WshShell.Run(scite_path,1,false);
