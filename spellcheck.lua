--[[--------------------------------------------------
spellcheck.lua
Authors: mozers�
Based: Matt White <http://luahunspell.googlecode.com>
Version 2.0.1
------------------------------------------------------
� ���������� (����� ���� Tools) ������� �������� ���������� ���������� ��� ������ �, �����������, ��� ����� ����.
------------------------------------------------------
���������� hunspell.dll https://bitbucket.org/scite-ru/scripts/raw/scripts/hunspell.rar
������������� � $(SciteDefaultHome)\tools\LuaLib

��������� ������ ������� http://wiki.services.openoffice.org/wiki/Dictionaries
��� ������ https://bitbucket.org/scite-ru/scripts/raw/scripts/spellcheck_dictionary.rar
������������� ru_RU.aff � ru_RU.dic � ����� $(SciteDefaultHome)\dic\

��������� � ���� .properties:
  spellcheck.dictionaries=$(SciteDefaultHome)\dic
  spellcheck.language=ru_RU
  spellcheck.language.test=�����
  spellcheck.language.chars=�-��-�
  find.mark.7=#FF0000,squiggle

  command.parent.110.*=11
  command.name.110.*=Spelling
  command.110.*=SpellcheckTogge
  command.mode.110.*=subsystem:lua,savebefore:no
  command.shortcut.110.*=F9
  command.checked.110.*=$(spellcheck.on)

��������� � SciTEStartup.lua:
  dofile (props["SciteDefaultHome"].."\\tools\\spellcheck.lua")
--]]--------------------------------------------------
require("hunspell")

local mark_num = 7 -- ����� ������� ��� ������� ��������� ����
local spchk_buffer = {} -- ������ � ������� �������� �������� ����������

local function SpellcheckInit()
	local dic_path = props["spellcheck.dictionaries"] -- �������������� ��������
	local lng = props["spellcheck.language"] -- ���� �������������
	hunspell.init(dic_path.."\\"..lng..".aff", dic_path.."\\"..lng..".dic") -- ���� �� �-��� ���������� ���������, �� ����� ���� �� ��������� ���������� �������� ��������
	if not hunspell.spell(props["spellcheck.language.test"]) then -- ���� ����������� ����� �� ������� � �������, ��
		print("Spelling dictionary not work!")                    -- ������� ��������� �� ������
		return
	end
	spchk_buffer[props["FilePath"]] = true
	props["spellcheck.on"] = 1
end

-- ������� �������� ����������
local function SpellcheckRun()
	local word_start, word_end = 0, 0
	repeat
		word_start, word_end = editor:findtext("["..props["spellcheck.language.chars"].."]+", SCFIND_REGEXP, word_start)
		if word_start == nil then break end
		local word = editor:textrange(word_start, word_end)
		if tonumber(props["editor.unicode.mode"]) ~= IDM_ENCODING_DEFAULT then
			word = shell.from_utf8(word)
		end
		if not hunspell.spell(word) then
			EditorMarkText(word_start, word_end - word_start, mark_num)
		end
		word_start = word_end + 1
	until false
end

-- ������� ��� ����������� ��������� ����������� ��������� (����������� �� ����-����� �� ������������ �����)
AddEventHandler("OnDoubleClick", function()
	if not spchk_buffer[props["FilePath"]] then return end
	if scite.SendEditor(SCI_INDICATORVALUEAT, mark_num, editor.CurrentPos-1) == 0 then return end
	if editor:AutoCActive() then return end
	editor.AutoCAutoHide = false
	local word = GetCurrentWord()
	local word_len = #word
	if tonumber(props["editor.unicode.mode"]) ~= IDM_ENCODING_DEFAULT then
		word = shell.from_utf8(word)
	end
	if hunspell.spell(word) == false then
		local sug = hunspell.suggest(word)
		if #sug > 0 then
if tonumber(props["editor.unicode.mode"]) ~= IDM_ENCODING_DEFAULT then
			for i = 0, #sug do
				sug[i] = shell.to_utf8(sug[i])
			end
end
			editor:AutoCShow(word_len, table.concat(sug, " "))
		end
	end
end)

-- ����������� �������� � ������ ����
local function SpellcheckIndicator()
	if spchk_buffer[props["FilePath"]] then
		props["spellcheck.on"] = 1
		scite.Perform('reloadproperties:')
	else
		if tonumber(props["spellcheck.on"]) == 1 then
			props["spellcheck.on"] = 0
			scite.Perform('reloadproperties:')
		end
	end
	-- scite.CheckMenus() -- �� ��������!
	-- scite.Perform('reloadproperties:') -- �������� ��������� ���� �������� (����� ��������� �������� � ���� �� ��������)
end
AddEventHandler("OnSwitchFile", SpellcheckIndicator)
AddEventHandler("OnOpen", SpellcheckIndicator)
AddEventHandler("OnClose", SpellcheckIndicator)

-- ��������/���������� �������� ���������� � ������� ������
function SpellcheckTogge()
	if props['FileName'] == '' then return end
	if tonumber(props["spellcheck.on"]) == 1 then
		props["spellcheck.on"] = 0
		spchk_buffer[props["FilePath"]] = nil
		EditorClearMarks(mark_num)
	else
		SpellcheckInit()
		SpellcheckRun()
	end
end

-- ������������ ������������ ��� ����� ������
AddEventHandler("OnChar", function(char)
	if spchk_buffer[props["FilePath"]] and char == " " then
		SpellcheckRun()
	end
end)
